﻿using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;

namespace ApelMusicBackEnd.Controllers
{
    
    [ApiController]
    [Route("api/[controller]")]
    public class ClassController : Controller
    {
        IClassService classService;
        public ClassController(IClassService classService)
        {
            this.classService = classService;
        }
        [HttpGet("SelectTop")]
        public async Task<IActionResult> SelectTop(int id,int excludeId)
        {
            var result = await classService.SelectTopClass(id,excludeId);
            return Json(result);
        }

        [HttpGet("SelectPageData")]
        public async Task<IActionResult> SelectPageData(int id)
        {
            var result = await classService.SelectPageData(id);
            return Json(result);
        }
    }
}
