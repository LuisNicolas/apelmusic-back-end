﻿using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MyClassController : Controller
    {
        IMyClassService myClassService;

        public MyClassController(IMyClassService myClassService)
        {
            this.myClassService = myClassService;
        }

        [HttpGet("SelectClass")]
        public async Task<IActionResult> SelectClass(string email)
        {
            var result = await myClassService.SelectClass(email);
            return Json(result);
        }

        

    }
}
