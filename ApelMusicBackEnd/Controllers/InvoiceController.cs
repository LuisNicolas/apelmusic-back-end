﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using ApelMusicBackEnd.Service.Interface.Services;
using ApelMusicBackEnd.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class InvoiceController : Controller
    {
        IInvoiceService invoiceService;
        public InvoiceController(IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
        }
    
        
        [HttpPost("Insert")]
        public async Task Insert([FromBody] InvoiceSubmitModel data)
        {
            await invoiceService.Insert(data);
        }

        
        [HttpGet("SelectAll")]
        public async Task<IActionResult> SelectAll(string email)
        {
            var result = await invoiceService.SelectAll(email);
            return Json(result);
        }

        [HttpGet("SelectItem")]
        public async Task<IActionResult> SelectItem(int id)
        {
            var result = await invoiceService.SelectItem(id);
            return Json(result);
        }

        [HttpGet("SelectDetail")]
        public async Task<IActionResult> SelectDetail(int id)
        {
            var result = await invoiceService.SelectDetail(id);
            return Json(result);
        }
    }
}
