﻿using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FooterController : Controller
    {
        IFooterService footerService;
        public FooterController(IFooterService footerService) {
            this.footerService = footerService;
        }

        [HttpGet("Content")]
        public async Task<IActionResult> SelectAboutTitle()
        {
            var result = await footerService.SelectAboutTitle();
            return Json(result);
        }

        [HttpGet("Category")]
        public async Task<IActionResult> SelectFooterCategory()
        {
            var result = await footerService.SelectFooterCategory();
            return Json(result);
        }
    }
}
