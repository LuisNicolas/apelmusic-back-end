﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentMethodController : Controller
    {
        IPaymentMethodService paymentMethodService;

        public PaymentMethodController(IPaymentMethodService paymentMethodService)
        {
            this.paymentMethodService = paymentMethodService;
        }

        [HttpPost("Insert")]
        public async Task Insert([FromForm] PaymentMethodEntity data)
        {
            await paymentMethodService.Insert(data);
        }

        [HttpGet("SelectAll")]
        public async Task<IActionResult> SelectAll()
        {
            var result = await paymentMethodService.SelectAll();
            return Json(result);
        }

        [HttpDelete("Delete")]
        public async Task Delete(int id)
        {
            await paymentMethodService.Delete(id);
        }

        [HttpPatch("Update")]
        public async Task Update([FromForm] PaymentMethodEntity data)
        {
            await paymentMethodService.Update(data);
        }
    }
}
