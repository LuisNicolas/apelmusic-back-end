﻿using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LandingPageController : Controller
    {
        ILandingPageService landingPageService;
        public LandingPageController(ILandingPageService landingPageService) { 
            this.landingPageService = landingPageService;
        }
        [HttpGet("GetData")]
        public async Task<IActionResult> GetData()
        {
            var result = await landingPageService.SelectLandingPageData();
            return Json(result);
        }

        [HttpGet("GetCardData")]
        public async Task<IActionResult> GetCardData()
        {
            var result = await landingPageService.SelectLPCardData();
            return Json(result);
        }
    }
}
