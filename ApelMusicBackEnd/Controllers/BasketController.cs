﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using ApelMusicBackEnd.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class BasketController : Controller
    {
        IBasketService basketService;
        public BasketController(IBasketService basketService)
        {
            this.basketService = basketService;
        }
        [HttpPost("Insert")]
        public async Task Insert([FromForm] BasketEntity data)
        {
            await basketService.Insert(data);
        }

        [HttpGet("SelectAll")]
        public async Task<IActionResult> SelectAll(string email)
        {
            var result = await basketService.SelectAll(email);
            return Json(result);
        }

        [HttpDelete("Delete")]
        public async Task Delete(int id)
        {
            await basketService.Delete(id);
        }
    }
}
