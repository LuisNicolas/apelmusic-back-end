﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using ApelMusicBackEnd.Service.Interface.Services;
using ApelMusicBackEnd.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApelMusicBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : Controller
    {
        IRegisterService registerService;
        public RegisterController(IRegisterService registerService)
        {
            this.registerService = registerService;
        }
        [HttpPost("Insert")]
        public async Task Insert(UserSubmitEntity data)
        {
            await registerService.Insert(data);
        }

        [HttpPost("ActivateAccount")]
        public async Task UpddateUserStatus(string email)
        {
            await registerService.UpdateUserStatus(email);
        }
        [HttpPost("Login")]
        public async Task<LoginResult> Login(LoginSubmitEntity data)
        {
            return await registerService.Login(data);
        }
        [HttpPost("ResetPasswordEmail")]
        public async Task ResetPassword(string email)
        {
            await registerService.ResetPassword(email);
        }

        [HttpPatch("UpdatePassword")]
        public async Task UpdatePassword(UpdatePasswordSubmitEntity data)
        {
            await registerService.UpdatePassword(data);
        }
    }
}
