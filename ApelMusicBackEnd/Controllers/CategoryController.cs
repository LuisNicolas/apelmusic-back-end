﻿using ApelMusicBackEnd.Service.Interface.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;

namespace ApelMusicBackEnd.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        ICategoryService categoryService;
        public CategoryController(ICategoryService categoryService) {
            this.categoryService = categoryService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var result = await categoryService.SelectAllCategory();
            return Json(result);
        }

        [HttpGet("SelectPageData")]
        public async Task<IActionResult> SelectPageData(int id)
        {
            var result = await categoryService.SelectPageData(id);
            return Json(result);
        }

        [HttpGet("SelectCategoryName")]
        public async Task<IActionResult> SelectCategoryName(int id)
        {
            var result = await categoryService.SelectCategoryName(id);
            return Json(result);
        }
    }
}
