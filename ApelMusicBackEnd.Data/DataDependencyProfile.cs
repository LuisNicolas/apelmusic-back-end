﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data
{
    public class DataDependencyProfile
    {
        public static void Register(IConfiguration _Configuration, IServiceCollection services)
        {
            services.AddScoped<IFooterRepository,FooterRepository>();
            services.AddScoped<ILandingPageRepository,LandingPageRepository>();
            services.AddScoped<ICategoryRepository,CategoryRepository>();
            services.AddScoped<IClassRepository,ClassRepository>();
            services.AddScoped<IPaymentMethodRepository,PaymentMethodRepository>();
            services.AddScoped<IRegisterRepository, RegisterRepository>();
            services.AddScoped<IBasketRepository, BasketRepository>();
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<IMyClassRepository, MyClassRepository>();
        }
    }
}
