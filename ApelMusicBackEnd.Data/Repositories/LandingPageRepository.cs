﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class LandingPageRepository : ILandingPageRepository
    {
        IConfiguration _configuration;
        public LandingPageRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<LandingPageEntity> SelectLandingPageData()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""LandingPage""";

                var result = await db.QueryAsync<LandingPageEntity>(query);
                return result.FirstOrDefault();
            }
        }

        public async Task<List<LPCardEntity>> SelectLPCardData()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""LPCard""";

                var result = await db.QueryAsync<LPCardEntity>(query);
                return result.ToList();
            }
        }
    }
}
