﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using Dapper;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using ApelMusicBackEnd.Data.Interface.Repositories;
using Microsoft.Data.SqlClient;
using ApelMusicBackEnd.Model.Entities;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class FooterRepository : IFooterRepository
    {
        protected IConfiguration _configuration;

        public FooterRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<FooterEntity> SelectAboutTitle()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT AboutTitle,AboutContent,ProductTitle,AddressTitle,AddressContent,ContactTitle FROM ""footer""";

                var result = await db.QueryAsync<FooterEntity>(query);
                return result.FirstOrDefault();
            }
        }

        public async Task<List<CategoryEntity>> SelectFooterCategory()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""Kategori""";

                var result = await db.QueryAsync<CategoryEntity>(query);
                return result.ToList();
            }
        }
    }
}
