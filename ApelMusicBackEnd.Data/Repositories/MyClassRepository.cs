﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApelMusicBackEnd.Model.Entities;
using Microsoft.Data.SqlClient;
using Dapper;
using ApelMusicBackEnd.Data.Interface.Repositories;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class MyClassRepository : IMyClassRepository
    {
        protected IConfiguration _configuration;

        public MyClassRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        //My Class tampilkan semua
        public async Task<List<MyClassEntity>> SelectClass(string email)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT Kelas.Nama, Kelas.Gambar, Kategori.Nama as Kategori, InvoiceItem.Jadwal
                            FROM [User]
                            INNER JOIN Invoice ON [User].id = Invoice.UserId
                            INNER JOIN InvoiceItem ON Invoice.Id = InvoiceItem.InvoiceId
                            INNER JOIN Kelas ON InvoiceItem.KelasId = Kelas.Id
                            INNER JOIN Kategori ON Kelas.KategoriId = Kategori.Id
                            WHERE [User].Email = @email";

                var result = await db.QueryAsync<MyClassEntity>(query, new { email });
                return result.ToList();
            }
        }

    }
}
