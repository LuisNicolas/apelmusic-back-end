﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class PaymentMethodRepository : IPaymentMethodRepository
    {
        IConfiguration _configuration;
        public PaymentMethodRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task Insert(PaymentMethodEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"INSERT INTO ""MetodePembayaran""(Name,ImageFile) VALUES(@Name,@ImageFile)";
                var param = new { Name = data.Name, ImageFile = data.ImageFile };

                await db.ExecuteAsync(query,param);
            }
        }

        public async Task<List<PaymentMethodEntity>> SelectAll()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""MetodePembayaran""";

                var result = await db.QueryAsync<PaymentMethodEntity>(query);
                return result.ToList();
            }
        }

        public async Task Delete(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"DELETE FROM ""MetodePembayaran"" WHERE Id = @id";
                var param = new { id = id};

                await db.ExecuteAsync(query, param);
            }
        }

        public async Task Update(PaymentMethodEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"UPDATE ""MetodePembayaran"" SET Name = @Name, ImageFile = @ImageFile WHERE Id = @id";
                var param = new { Name = data.Name, ImageFile = data.ImageFile, id = data.Id };

                await db.ExecuteAsync(query, param);
            }
        }
    }
}
