﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class ClassRepository : IClassRepository
    {
        IConfiguration _configuration;
        public ClassRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<List<ClassEntity>> SelectTopClass(int id, int excludeId)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT TOP 6 a.*, b.Nama AS KategoriName FROM ""Kelas"" AS a INNER JOIN ""Kategori"" AS b ON a.KategoriId = b.Id";
                if (id != 0)
                {
                    query += @" WHERE a.KategoriId = @id";
                    if(excludeId != 0)
                    {
                        query += @" AND a.Id!=@excludeId";
                    }
                }
                query += @" ORDER BY a.Terjual DESC";
                var param = new { id = id , excludeId = excludeId};
                var result = await db.QueryAsync<ClassEntity>(query,param);
                return result.ToList();
            }
        }

        public async Task <ClassEntity> SelectPageData(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT a.*, b.Nama AS KategoriName FROM ""Kelas"" AS a INNER JOIN ""Kategori"" AS b ON a.KategoriId = b.Id WHERE a.Id = @id";
                var param = new { id = id };
                var result = await db.QueryAsync<ClassEntity>(query, param);
                return result.First();
            }
        }
    }
}
