﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        IConfiguration _configuration;
        public CategoryRepository(IConfiguration configuration) {
            _configuration = configuration;
        }
        public async Task<List<CategoryEntity>> SelectAllCategory()
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""Kategori""";

                var result = await db.QueryAsync<CategoryEntity>(query);
                return result.ToList();
            }
        }
        public async Task<CategoryEntity> SelectPageData(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT * FROM ""Kategori"" WHERE Id = @id";
                var param = new { id = id };

                var result = await db.QueryAsync<CategoryEntity>(query,param);
                return result.First();
            }
        }

        public async Task<string> SelectCategoryName(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT Nama FROM ""Kategori"" WHERE Id = @id";
                var param = new { id = id };

                var result = await db.QueryAsync<string>(query, param);
                return result.First();
            }
        }
    }
}
