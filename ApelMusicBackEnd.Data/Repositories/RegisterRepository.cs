﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using Dapper;
using MailKit.Net.Smtp;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using Utility;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class RegisterRepository : IRegisterRepository
    {
        protected IConfiguration _configuration;

        public RegisterRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task Insert(UserSubmitEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var checkIfEmailExistQuery = @"SELECT COUNT(*) FROM [User] WHERE email = @email";
                        var emailExist = await db.ExecuteScalarAsync<int>(checkIfEmailExistQuery, new { email = data.Email }, transaction);
                        if (emailExist > 0)
                        {
                            throw new Exception("Email already exist");
                        }

                        var passwordSalt = HashUtility.GenerateSalt();
                        var passwordHash = HashUtility.GenerateHash(data.Password, passwordSalt);

                        var insertQuery = @"
                    INSERT INTO [User] (username, email, hashedPassword, passwordSalt, role, isActive) 
                    VALUES (@username, @email, @hashedPassword, @passwordSalt, @role, @isActive)";

                        var result = await db.ExecuteAsync(insertQuery, new
                        {
                            username = data.Username,
                            email = data.Email,
                            hashedPassword = passwordHash,
                            passwordSalt = passwordSalt,
                            role = 1,
                            isActive = 0
                        }, transaction);

                        transaction.Commit();
                        SendConfirmationEmail(data.Email, data.Username);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public async Task UpdateUserStatus(string email)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var updateQuery = @"UPDATE [User] SET isActive = 1 WHERE email = @email";
                        var result = await db.ExecuteAsync(updateQuery, new { email = email }, transaction);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }
        public async Task SendConfirmationEmail(string email, string username)
        {
            var baseURL = _configuration.GetSection("AppSettings")["BaseURL"];
            
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Apel Music", "farhancodingid@gmail.com"));
            message.To.Add(new MailboxAddress(username, email));
            message.Subject = "Account Confirmation";
            //var text = $"Dear {username},\n\nThank you for registering with Apel Music. Please click the following link to confirm your email address:\n\n{baseURL}/EmailConfirmSuccess/{email}\n\nBest regards,\nApel Music";
            //message.Body = new TextPart("plain")
            //{
            //    Text = text

            //};

            var link = $"{baseURL}/EmailConfirmSuccess/{email}";
            var text = $@"<p>Dear {username},</p>
                 <p>Thank you for registering with Apel Music. Please click the following button to confirm your email address:</p>
                 <p><a href=""{link}"" target=""_blank""><button style=""background-color: #4CAF50; color: white; padding: 14px 20px; margin: 8px 0; border: none; cursor: pointer; border-radius: 4px;"">Confirm Email</button></a></p>
                 <p>Best regards,</p>
                 <p>Apel Music</p>";

            message.Body = new TextPart("html")
            {
                Text = text
            };

            string smtpServer = _configuration["EmailSettings:SmtpServer"];
            int smtpPort = int.Parse(_configuration["EmailSettings:SmtpPort"]);
            bool useSsl = bool.Parse(_configuration["EmailSettings:UseSsl"]);
            string senderEmail = _configuration["EmailSettings:SenderEmail"];
            string senderPassword = _configuration["EmailSettings:SenderPassword"];

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(smtpServer, smtpPort, useSsl);
                await client.AuthenticateAsync(senderEmail, senderPassword);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
        }

        public async Task<LoginResult> Login(LoginSubmitEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                var getUserQuery = @"SELECT * FROM [User] WHERE email = @email";
                var user = await db.QueryFirstOrDefaultAsync<UserEntity>(getUserQuery, new { email = data.Email });
                

                if (user == null)
                {
                    throw new Exception("User not found");
                }

                var passwordHash = HashUtility.GenerateHash(data.Password, user.PasswordSalt);

                if (!passwordHash.SequenceEqual(user.HashedPassword))
                {
                    throw new Exception("Incorrect password");
                }

                if (user.IsActive == 0)
                {
                    throw new Exception("User is not active");
                }

                var tokenHandler = new JwtSecurityTokenHandler();
                var secretKey = _configuration.GetSection("JwtSettings:SecretKey").Value;
                var key = Encoding.ASCII.GetBytes(secretKey);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role.ToString()),
                    }),
                    Expires = DateTime.UtcNow.AddDays(90),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);

                return new LoginResult { Token = tokenString };
            }
           
        }
        public async Task<bool> ResetPassword(string email)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var checkIfEmailExistQuery = @"SELECT COUNT(*) FROM [User] WHERE email = @email";
                        var emailExist = await db.ExecuteScalarAsync<int>(checkIfEmailExistQuery, new { email = email }, transaction);
                        if (emailExist == 0)
                        {
                            throw new Exception("Email not found");
                        }

                        SendResetPasswordEmail(email);

                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public async Task SendResetPasswordEmail(string email)
        {
            var baseURL = _configuration.GetSection("AppSettings")["BaseURL"];

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Apel Music", "farhancodingid@gmail.com"));
            message.To.Add(new MailboxAddress("", email));
            message.Subject = "Reset Password";
            //var text = "Dear user,\n\nYou have requested to reset your password. Please click the following link to reset your password:\n\n" +
            //baseURL + "/ResetPasswordNewPass/" + email + "\n\n" +
            //"Best regards,\nApel Music";
            //message.Body = new TextPart("plain")
            //{
            //    Text = text
            //};
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<p>Dear user,</p>" +
                "<p>You have requested to reset your password. Please click the following link to reset your password:</p>" +
                $"<a href='{baseURL}/ResetPasswordNewPass/{email}'><button style='background-color: #4CAF50; color: white; padding: 12px 20px; border: none; border-radius: 4px; cursor: pointer;'>Reset Password</button></a>" +
                "<p>Best regards,<br>Apel Music</p>";
            message.Body = bodyBuilder.ToMessageBody();

            string smtpServer = _configuration["EmailSettings:SmtpServer"];
            int smtpPort = int.Parse(_configuration["EmailSettings:SmtpPort"]);
            bool useSsl = bool.Parse(_configuration["EmailSettings:UseSsl"]);
            string senderEmail = _configuration["EmailSettings:SenderEmail"];
            string senderPassword = _configuration["EmailSettings:SenderPassword"];

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(smtpServer, smtpPort, useSsl);
                await client.AuthenticateAsync(senderEmail, senderPassword);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
        }
        public async Task UpdatePassword(UpdatePasswordSubmitEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var passwordSalt = HashUtility.GenerateSalt();
                        var passwordHash = HashUtility.GenerateHash(data.password, passwordSalt);

                        var updateQuery = @"UPDATE [User] SET hashedPassword = @hashedPassword, passwordSalt = @passwordSalt WHERE email = @email";
                        var result = await db.ExecuteAsync(updateQuery, new
                        {
                            hashedPassword = passwordHash,
                            passwordSalt = passwordSalt,
                            email = data.email
                        }, transaction);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }
    }
}