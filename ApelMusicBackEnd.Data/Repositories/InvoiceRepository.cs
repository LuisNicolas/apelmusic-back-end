﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class InvoiceRepository:IInvoiceRepository
    {
        IConfiguration _configuration;
        public InvoiceRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task Insert(InvoiceSubmitModel data)
        {
            int id;
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var queryUser = @"SELECT Id FROM [User] WHERE email = @email";
                var queryUserParam = new { email = data.Email };
                var result = db.QueryAsync<int>(queryUser, queryUserParam);
                id = result.Result.First();
            }

            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                if (db.State != ConnectionState.Open)
                {
                    await db.OpenAsync();
                }

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var queryInvoice = @"INSERT INTO ""Invoice""(TglBeli,TotalHarga,UserId) VALUES(@TglBeli,@Totalharga,@UserId);SELECT SCOPE_IDENTITY()";
                        var paramInvoice = new { TglBeli = DateTime.Today, TotalHarga = data.TotalHarga, UserId = id };

                        var InvoiceId = await db.ExecuteScalarAsync<int>(queryInvoice, paramInvoice,transaction);

                        for(int x=0; x<data.Id.Count;x++)
                        {
                            var queryBasket = @"SELECT * FROM ""Keranjang"" WHERE Id = @Id";
                            var queryBasketParam = new { Id = data.Id[x] };
                            var resultBasket = db.QueryAsync<BasketEntity>(queryBasket, queryBasketParam,transaction);
                            var basketItem = resultBasket.Result.First();

                            var queryInvoiceItem = @"INSERT INTO ""InvoiceItem""(InvoiceId,KelasId,Jadwal) VALUES(@InvoiceId,@KelasId,@Jadwal);UPDATE ""Kelas"" SET Terjual = Terjual + 1 WHERE Id=@KelasId; DELETE FROM ""Keranjang"" WHERE Id = @BasketId;";
                            var paramInvoiceItem = new { InvoiceId = InvoiceId, KelasId = basketItem.KelasId, Jadwal = basketItem.Jadwal, BasketId = basketItem.Id };

                            await db.ExecuteAsync(queryInvoiceItem, paramInvoiceItem,transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public async Task<List<InvoiceEntity>> SelectAll(string email)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                try
                {
                    var roleQuery = @"SELECT role FROM ""User"" WHERE email = @email";
                    var param = new { email = email };

                    var role = await db.QueryFirstAsync<int>(roleQuery, param);

                    var query = @"SELECT a.Id, a.TglBeli, COUNT(c.Id) AS JumlahKursus,a.TotalHarga FROM ""Invoice"" AS a INNER JOIN ""User"" AS b ON a.UserId = b.id INNER JOIN ""InvoiceItem"" AS c ON a.Id = c.InvoiceId";
                    if (role == 0)
                    {
                        query += @" GROUP BY a.Id,a.TglBeli,a.TotalHarga";
                    }
                    else
                    {
                        query += @" WHERE b.email = @email GROUP BY a.Id,a.TglBeli,a.TotalHarga";
                    }

                    var result = await db.QueryAsync<InvoiceEntity>(query, param);
                    return result.ToList();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<List<InvoiceItemEntity>> SelectItem(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT a.Id, b.Nama AS Name, c.Nama AS Category, a.Jadwal, b.Harga  FROM ""InvoiceItem"" AS a INNER JOIN ""Kelas"" AS b ON a.KelasId = b.id INNER JOIN ""Kategori"" AS c ON b.KategoriId = c.Id  WHERE a.InvoiceId = @id";
                var param = new { id = id };

                var result = await db.QueryAsync<InvoiceItemEntity>(query, param);
                return result.ToList();
            }
        }

        public async Task<InvoiceDetailEntity> SelectDetail(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT Id,TglBeli,TotalHarga FROM ""Invoice"" WHERE Id = @id";
                var param = new { id = id };

                var result = await db.QueryAsync<InvoiceDetailEntity>(query, param);
                return result.First();
            }
        }
    }
}
