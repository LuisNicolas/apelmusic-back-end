﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Repositories
{
    public class BasketRepository : IBasketRepository
    {
        IConfiguration _configuration;
        public BasketRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task Insert(BasketEntity data)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {

                var queryUser = @"SELECT Id FROM [User] WHERE email = @email";
                var queryUserParam = new { email = data.Email };
                var result = db.QueryAsync<int>(queryUser, queryUserParam);
                int id = result.Result.First();

                var query = @"INSERT INTO ""Keranjang""(KelasId,Jadwal,UserId) VALUES(@KelasId,@Jadwal,@UserId)";
                var param = new { KelasId = data.KelasId, Jadwal = data.Jadwal, UserId = id };

                await db.ExecuteAsync(query, param);
            }
        }

        public async Task<List<CompleteBasketEntity>> SelectAll(string email)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"SELECT a.Id, a.KelasId, b.Nama AS KelasName,b.Gambar, c.Nama AS KategoriName,b.Harga,a.Jadwal FROM ((""Keranjang"" AS a INNER JOIN ""Kelas"" AS b ON a.KelasId = b.Id) INNER JOIN ""Kategori"" AS c ON b.KategoriId=c.Id) INNER JOIN ""User"" AS d ON a.UserId = d.Id WHERE d.email=@email";
                var param = new { email = email };

                var result = await db.QueryAsync<CompleteBasketEntity>(query,param);
                return result.ToList();
            }
        }
        public async Task Delete(int id)
        {
            using (var db = new SqlConnection(_configuration.GetConnectionString("connstring")))
            {
                var query = @"DELETE FROM ""Keranjang"" WHERE Id = @id";
                var param = new { id = id };

                await db.ExecuteAsync(query, param);
            }
        }
    }
}
