﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using ApelMusicBackEnd.Data.Interface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class PaymentMethodService : IPaymentMethodService
    {
        IPaymentMethodRepository paymentMethodRepository;
        public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository) {
            this.paymentMethodRepository = paymentMethodRepository;
        }
        public async Task Insert(PaymentMethodEntity data)
        {
            await paymentMethodRepository.Insert(data);
        }

        public async Task <List<PaymentMethodEntity>>SelectAll()
        {
            return await paymentMethodRepository.SelectAll();
        }

        public async Task Delete(int id)
        {
            await paymentMethodRepository.Delete(id);
        }

        public async Task Update(PaymentMethodEntity data)
        {
            await paymentMethodRepository.Update(data);
        }
    }
}
