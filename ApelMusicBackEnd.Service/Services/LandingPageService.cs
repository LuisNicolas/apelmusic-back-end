﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class LandingPageService:ILandingPageService
    {
        ILandingPageRepository landingPageRepository;
        public LandingPageService(ILandingPageRepository landingPageRepository) {
            this.landingPageRepository = landingPageRepository;
        }

        public async Task<LandingPageEntity> SelectLandingPageData()
        {
            return await landingPageRepository.SelectLandingPageData();
        }

        public async Task<List<LPCardEntity>> SelectLPCardData()
        {
            return await landingPageRepository.SelectLPCardData();

        }
    }
}
