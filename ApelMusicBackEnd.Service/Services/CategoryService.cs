﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class CategoryService:ICategoryService
    {
        ICategoryRepository _repository;
        public CategoryService(ICategoryRepository categoryRepository) {
            _repository= categoryRepository;
        }

        public async Task<List<CategoryEntity>> SelectAllCategory()
        {
            return await _repository.SelectAllCategory();
        }

        public async Task<CategoryEntity> SelectPageData(int id)
        {
            return await _repository.SelectPageData(id);
        }

        public async Task<string> SelectCategoryName(int id)
        {
            return await _repository.SelectCategoryName(id);
        }
    }
}
