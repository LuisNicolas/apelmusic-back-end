﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class ClassService:IClassService
    {
        IClassRepository classRepository;
        public ClassService(IClassRepository classRepository) {
            this.classRepository = classRepository;
        }

        public async Task<List<ClassEntity>> SelectTopClass(int id, int excludeId)
        {
            return await this.classRepository.SelectTopClass(id,excludeId);
        }

        public async Task<ClassEntity> SelectPageData(int id)
        {
            return await classRepository.SelectPageData(id);
        }
    }
}
