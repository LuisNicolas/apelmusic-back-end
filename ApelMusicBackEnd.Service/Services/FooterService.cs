﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class FooterService : IFooterService
    {
        IFooterRepository footerRepository;
        public FooterService(IFooterRepository footerRepository) {
            this.footerRepository = footerRepository;
        }
        public async Task<FooterEntity> SelectAboutTitle()
        {
            return await footerRepository.SelectAboutTitle();
        }

        public async Task<List<CategoryEntity>> SelectFooterCategory()
        {
            return await footerRepository.SelectFooterCategory();
        }
    }
}
