﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class InvoiceService : IInvoiceService
    {
        IInvoiceRepository _invoiceRepository;
        public InvoiceService(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        public async Task Insert(InvoiceSubmitModel data)
        {
            await _invoiceRepository.Insert(data);
        }

        public async Task<List<InvoiceEntity>> SelectAll(string email)
        {
            return await _invoiceRepository.SelectAll(email);
        }

        public async Task<List<InvoiceItemEntity>> SelectItem(int id)
        {
            return await _invoiceRepository.SelectItem(id);
        }

        public async Task<InvoiceDetailEntity> SelectDetail(int id)
        {
            return await _invoiceRepository.SelectDetail(id);
        }
    }
}
