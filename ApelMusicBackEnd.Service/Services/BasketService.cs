﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class BasketService:IBasketService
    {
        IBasketRepository basketRepository;
        public BasketService(IBasketRepository basketRepository) {
            this.basketRepository = basketRepository;
        }

        public async Task Insert(BasketEntity data)
        {
            await basketRepository.Insert(data);
        }

        public async Task<List<CompleteBasketEntity>> SelectAll(string email)
        {
            return await basketRepository.SelectAll(email);
        }

        public async Task Delete(int id)
        {
            await basketRepository.Delete(id);
        }
    }
}
