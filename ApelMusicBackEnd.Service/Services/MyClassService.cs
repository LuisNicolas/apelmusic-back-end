﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Service.Interface.Services;

namespace ApelMusicBackEnd.Service.Services
{
    public class MyClassService : IMyClassService
    {
        IMyClassRepository myClassRepository;
        public MyClassService(IMyClassRepository myClassRepository)
        {
            this.myClassRepository = myClassRepository;
        }
        public async Task<List<MyClassEntity>> SelectClass(string email)
        {
            return await myClassRepository.SelectClass(email);
        }
    }
}
