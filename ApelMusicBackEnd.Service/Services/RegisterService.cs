﻿using ApelMusicBackEnd.Data.Interface.Repositories;
using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using ApelMusicBackEnd.Service.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Services
{
    public class RegisterService : IRegisterService
    {
        IRegisterRepository registerRepository;
        public RegisterService(IRegisterRepository registerRepository)
        {
            this.registerRepository = registerRepository;
        }
        public async Task Insert(UserSubmitEntity data)
        {
            await registerRepository.Insert(data);
        }

        public async Task UpdateUserStatus(string email)
        {
            await registerRepository.UpdateUserStatus(email);
        }

        public async Task SendConfirmationEmail(string email, string username)
        {
            await registerRepository.SendConfirmationEmail(email, username);
        }

        public async Task<LoginResult> Login(LoginSubmitEntity data)
        {
            return await registerRepository.Login(data);
        }

        public async Task<bool> ResetPassword(string email)
        {
            return await registerRepository.ResetPassword(email);
        }
        
        public async Task SendResetPasswordEmail(string email)
        {
            await registerRepository.SendResetPasswordEmail(email);
        
        }

        public async Task UpdatePassword(UpdatePasswordSubmitEntity data)
        {
            await registerRepository.UpdatePassword(data);
        }
    }
}
