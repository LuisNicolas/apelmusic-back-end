﻿using ApelMusicBackEnd.Service.Interface.Services;
using ApelMusicBackEnd.Service.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service
{
    public class ServiceDependencyProfile
    {
        public static void Register(Microsoft.Extensions.Configuration.IConfiguration _Configuration, IServiceCollection services)
        {
            services.AddScoped<IFooterService, FooterService>();
            services.AddScoped<ILandingPageService, LandingPageService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IClassService, ClassService>();
            services.AddScoped<IPaymentMethodService, PaymentMethodService>();
            services.AddScoped<IRegisterService, RegisterService>();
            services.AddScoped<IBasketService, BasketService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IMyClassService, MyClassService>();

        }
    }
}
