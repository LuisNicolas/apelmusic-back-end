﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IFooterRepository
    {
        Task<FooterEntity> SelectAboutTitle();
        Task<List<CategoryEntity>> SelectFooterCategory();
    }
}
