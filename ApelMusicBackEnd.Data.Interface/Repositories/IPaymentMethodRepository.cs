﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IPaymentMethodRepository
    {
        Task Insert(PaymentMethodEntity data);
        Task<List<PaymentMethodEntity>> SelectAll();
        Task Delete(int id);
        Task Update(PaymentMethodEntity data);
    }
}
