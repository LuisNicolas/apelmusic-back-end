﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IRegisterRepository
    {
        Task Insert(UserSubmitEntity data);
        Task UpdateUserStatus(string email);

        Task SendConfirmationEmail(string email, string username);

        Task<LoginResult> Login(LoginSubmitEntity data);
        Task<bool> ResetPassword(string email);

        Task SendResetPasswordEmail(string email);

        Task UpdatePassword(UpdatePasswordSubmitEntity data);


    }
}
