﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IBasketRepository
    {
        Task Insert(BasketEntity data);
        Task<List<CompleteBasketEntity>> SelectAll(string email);
        Task Delete(int id);
    }
}
