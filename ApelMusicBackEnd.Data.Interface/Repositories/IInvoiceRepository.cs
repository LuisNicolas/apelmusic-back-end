﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IInvoiceRepository
    {
        Task Insert(InvoiceSubmitModel data);
        Task<List<InvoiceEntity>> SelectAll(string email);
        Task<List<InvoiceItemEntity>> SelectItem(int id);
        Task<InvoiceDetailEntity> SelectDetail(int id);
    }
}
