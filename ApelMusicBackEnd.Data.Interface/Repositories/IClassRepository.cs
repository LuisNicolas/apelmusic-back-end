﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Data.Interface.Repositories
{
    public interface IClassRepository
    {
        Task<List<ClassEntity>> SelectTopClass(int id, int excludeId);
        Task<ClassEntity> SelectPageData(int id);
    }
}
