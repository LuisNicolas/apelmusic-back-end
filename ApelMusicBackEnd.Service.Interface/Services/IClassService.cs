﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface IClassService
    {
        Task<List<ClassEntity>> SelectTopClass(int id, int excludeId);
        Task<ClassEntity> SelectPageData(int id);
    }
}
