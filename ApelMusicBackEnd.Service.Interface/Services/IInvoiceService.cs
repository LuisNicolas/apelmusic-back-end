﻿using ApelMusicBackEnd.Model.Entities;
using ApelMusicBackEnd.Model.Entities.SubmitModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface IInvoiceService
    {
        Task Insert(InvoiceSubmitModel data);
        Task<List<InvoiceEntity>> SelectAll(string email);
        Task<List<InvoiceItemEntity>> SelectItem(int id);
        Task<InvoiceDetailEntity> SelectDetail(int id);
    }
}
