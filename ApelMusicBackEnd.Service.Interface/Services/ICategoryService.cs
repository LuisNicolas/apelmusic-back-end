﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface ICategoryService
    {
        Task<List<CategoryEntity>> SelectAllCategory();
        Task<CategoryEntity> SelectPageData(int id);
        Task<string> SelectCategoryName(int id);
    }
}
