﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface IFooterService
    {
        Task<FooterEntity> SelectAboutTitle();
        Task<List<CategoryEntity>> SelectFooterCategory();
    }
}
