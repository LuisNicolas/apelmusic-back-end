﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface IPaymentMethodService
    {
        Task Insert(PaymentMethodEntity data);
        Task<List<PaymentMethodEntity>> SelectAll();
        Task Delete(int id);
        Task Update(PaymentMethodEntity data);
    }
}
