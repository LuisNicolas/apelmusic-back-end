﻿using ApelMusicBackEnd.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Service.Interface.Services
{
    public interface IBasketService
    {
        Task Insert(BasketEntity data);
        Task<List<CompleteBasketEntity>>SelectAll(string email);
        Task Delete(int id);
    }
}
