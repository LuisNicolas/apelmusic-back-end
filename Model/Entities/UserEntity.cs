﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class UserEntity
    {
        
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public byte[] HashedPassword { get; set; }

        public byte[] PasswordSalt { get; set; }

        public int Role { get; set; }

        public int IsActive { get; set; }
    }
}
