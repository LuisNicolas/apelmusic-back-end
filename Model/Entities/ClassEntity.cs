﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class ClassEntity
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public int KategoriId { get; set; }
        public float Harga { get; set; }
        public string Gambar { get; set; }
        public string Deskripsi { get; set; }
        public int Terjual { get; set; }
        public string KategoriName { get; set; }
    }
}
