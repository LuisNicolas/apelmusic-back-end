﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class InvoiceEntity
    {
        public int Id { get; set; }
        public DateTime TglBeli { get; set; }
        public int JumlahKursus { get; set; }
        public int TotalHarga { get; set; }
    }
}
