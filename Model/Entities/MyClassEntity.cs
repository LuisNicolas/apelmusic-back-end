﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class MyClassEntity
    {
        public string Nama { get; set; }

        public string Gambar { get; set; }

        public string Kategori { get; set; }

        public DateTime Jadwal { get; set; }


    }
}
