﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class BasketEntity
    {
        public int Id { get; set; }
        public int KelasId { get; set; }
        public DateTime Jadwal { get; set; }
        public string Email { get; set; }
    }
}
