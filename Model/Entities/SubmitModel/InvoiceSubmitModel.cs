﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities.SubmitModel
{
    public class InvoiceSubmitModel
    {
        public List<int> Id { get; set; }
        public int TotalHarga { get; set; }
        public string Email { get; set; }
    }
}
