﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities.SubmitModel
{
    public class LoginSubmitEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
