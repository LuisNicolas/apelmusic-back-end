﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class LandingPageEntity
    {
        public string HeaderTitle { get; set; }
        public string HeaderSubtitle { get; set; }
        public string BenefitImage { get; set; }
        public string BenefitTitle { get; set; }
        public string BenefitContent { get;set; }
    }
}
