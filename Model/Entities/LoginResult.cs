﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class LoginResult
    {
        public string Token { get; set; }
    }
}
