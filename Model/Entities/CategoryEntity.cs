﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class CategoryEntity
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public string Gambar { get; set; }
        public string JudulLaman { get; set; }
        public string GambarLaman { get; set; }
        public string DeskripsiLaman { get; set; }
    }
}
