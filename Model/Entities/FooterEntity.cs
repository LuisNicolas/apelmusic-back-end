﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class FooterEntity
    {
        public string AboutTitle { get; set; }
        public string AboutContent { get; set; }
        public string ProductTitle { get; set; }
        public string AddressTitle { get; set; }
        public string AddressContent { get; set; }
        public string ContactTitle { get; set; }
    }
}
