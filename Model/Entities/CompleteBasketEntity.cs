﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class CompleteBasketEntity
    {
        public int Id { get; set; }
        public int KelasId { get; set; }
        public string KelasName { get; set; }
        public string Gambar { get; set; }
        public string KategoriName { get; set; }
        public int Harga { get; set; }
        public DateTime Jadwal { get; set; }
    }
}
