﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelMusicBackEnd.Model.Entities
{
    public class LPCardEntity
    {
        public string CardHeader { get; set; }
        public string CardContent { get; set; }
    }
}
